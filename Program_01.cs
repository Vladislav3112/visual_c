﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Program
{
    static void Main(string[] args)
    {
        //1st hero
        Hero Mage = new Hero();
        System.Console.WriteLine("first hero:");
        
        int h1 = Mage.Health;
        int d1 = Mage.Damage;
        System.Console.WriteLine($"health:{h1} damage:{d1}");

        //2nd hero
        Hero Player = new Hero(5, 10);
        System.Console.WriteLine("second hero:");
        int h2 = Player.Health;
        int d2 = Player.Damage;

        //NPC

        NPC enemy = new NPC();

        NPC kind = new NPC(10000, 0, true);
        int npDamage = kind.Damage;
        int npEnemyDamage = enemy.Damage;
        bool npType = kind.Type;
        string type;
        if (npType) type = "Kind";
        else type = "Enemy";

        System.Console.WriteLine($"health:{h2} damage:{d2}");
        System.Console.WriteLine($"NPC type: {type}");
        System.Console.WriteLine($"NPC damage1:{npDamage} NPC damage2:{npEnemyDamage}");

        System.Console.ReadLine();

    }


    
}


public class Hero
{
    private int health;
    private int damage;

    public int Health {
        get { return health; }
        set { this.health = health; } 

    }
    public int  Damage
    {
        get { return damage; }
        set { this.damage = damage; }

    }
    public Hero()
    {
        damage = 1;
        health = 3;
    }
    public Hero(int h, int d)
    {
        damage = d;
        health = h;

    }
    


}
public class NPC : Hero
{

    private bool type;

    public bool Type
    {
        set { this.type = type; }
        get { return type; }
    }
    public NPC(int health, int damage, bool type)
        : base(health, damage)
    {

        this.type = type;

    }

    public NPC()
    {
        type = false;
    }

}